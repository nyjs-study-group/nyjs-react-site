import React from "react";

import { Header } from './components/header/header.component';

export default () => (
  <>
    <Header />
    <h1>Welcome to React App of </h1>
    <p>New York Javascript Study Group.</p>
  </>
);
