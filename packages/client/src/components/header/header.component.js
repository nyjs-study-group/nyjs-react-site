
import React from 'react';

import './header.component.css';

export const Header = props => {

  return (
    <div className="header header-space">
      <div className="title">Fracty</div>
    </div>
  );
}
